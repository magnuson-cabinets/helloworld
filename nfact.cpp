int nfact(int n) {
    if (n <= 1) return 1;
    return n*nfact(n-1);
}
